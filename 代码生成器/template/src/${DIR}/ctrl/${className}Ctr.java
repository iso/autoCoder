<#include "/macro.include"/>
package ${basepackage}.ctrl.${package};

import java.util.ArrayList;
import java.util.List;
import ${basepackage}.ctrl.AdminBaseController;
import ${basepackage}.jfinal.anatation.RouteBind;
import ${basepackage}.model.${package}.${className};
import ${basepackage}.vo.DataGrid;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
<#include "/java_imports.include"/>
@RouteBind(path = "${className}")
public class ${className}Ctrl extends AdminBaseController<${className}> {
	public ${className}Ctrl() {
		this.tableName = ${className}.tableName;
		modelClass = ${className}.class;
	}
	public void dataGrid() {
		List<Object> param = new ArrayList<Object>();
		StringBuffer where = new StringBuffer();
		/** 添加查询字段条件*/
		qryField(where, param);
		sortField(where);
		Page<Record> p = Db.paginate(getPageNo(), getPageSize(), "select <#list cls as col><#if col.Show??&&(col.Show==true)>t.${col.Field?upper_case},</#if></#list> 1 ", "from " + this.tableName+ " t where 1=1 " + where.toString(), param.toArray());
		DataGrid dg = new DataGrid();
		dg.setRows(p.getList());
		dg.setTotal(p.getTotalRow());
		this.renderJson(dg);
	}
}