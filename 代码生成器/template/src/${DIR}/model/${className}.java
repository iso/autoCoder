<#include "/macro.include"/>
package ${basepackagedot}.model.${package};

import ${basepackage}.jfinal.anatation.TableBind;
import com.jfinal.plugin.activerecord.Model;
<#include "/java_imports.include"/>
@TableBind(name="${tbname}")
public class ${className} extends Model<${className}> {
	public static final String tableName="${tbname}";
	public static ${className} dao=new ${className}();
}